# Audience Analysis
 
# Project: AI at the Edge: No Fail Approach
# Primary Audience Profile
- Maker
- Entrepreneurs
- IOT Developers
- Enterprise Developers


| Question | Response |
|----------|----------|
| What is the job function and role of the primary audience?| Best Guess |
| How will this audience use this document? | For reference while performing hands-on samples/demos |
| What is the educational level of this audience? | varies [ ] new [ ] can hack it [ ] some knowledge [ ] expert|
| How experienced are the members of this audience with IoT? | [ ] new [ ] can hack it [ ] some experience [ ] expert
| How experienced are the members of this audience with AI or machine Learning? | [ ] Beginner [ ] Intermediate [ ] Advanced |
| What is their work environment like? | [ ] Lab [ ] Desk/Office [ ] Remote |
| What is their interest level? | [ ] Low [ ] Medium [ ] High |
| What biases, preferences, or expectations, might they have? | [ ] Low [ ] Medium [ ] High |
| Which operating systems are they familiar? | List here:   |
| How much theory or “nice-to-know” information will they want? | Insert info here: |
| What other training will they receive in addition to the documentation? | [ ] samples [ ] tutorials [ ] training [ ] webinars [ ] other: |

# Secondary Audience

What is the job function of the secondary audience?

How will this audience use this document?

What is the educational level of this audience?

How experienced are the members of this audience?

How experienced are they with AI/Machine Learing?

How experienced are they with IoT?

Users experience vary. Level varies from
beginner to expert. These are school instructors.

What is their work environment like?
 
Educational office environment for administrative staff or
teaching environment for instructors, where they must have a basic computer for scheduling their courses with resources and standards.

Prototypical User example:

    The prototypical user will be a teacher who is preparing the coursework for the class. This may need approval or align with the teaching requirements.

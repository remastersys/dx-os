
# Solution
Solution = right people, right tools, right skills

# Examples 
## Non Technical Writer
This technical writer can create content, but maybe not really useful or even usable docs...

- doesn't know default apps, cannot edit or create this content
- if the writer cannot read a changelog, not so great release notes
- if the writer cannot install and experience the product/software, documentation is guided by the developer only, why bother with a writer
- technical editor for review is a good idea

# For side by side guidance
## Discourage side by side Linux/Windows instructions.

If necessary in a document - let it link to the section.

## Incorrect:
### Step 2: Windows Host
### Step 3: Linux Host
### Step 4: SSH

This is not the content strategy for this

# This is more appropriate:

## Step 2: Host Setup
### Option 1: Linux (alphabetical)
### Option 2: Windows

This is better.

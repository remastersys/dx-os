If you are on a project, this project may have many components and different configurations. 
However, if the documentation person cannot be like unraveling a ball of yarn or unknotting a fishing line, then maybe consider using a different resource.

We cannot continue to use non technical people for our documentation needs. The only time to use a non technical person is when you need to test the instructions and need fresh eyes.
Depending on the project a new fresh technical writer perspective may be valuable to see where non technical people trip or succeed.

Other than this, a more technical resource is needed to effectively produce and publish documentation.
Frustration is not something we need to overcome.

Best practices, use a technical writer who understand the content.
Use a technical writer who can write from experience with your product.
And use a technical writer who can validate the instructions after the fact.

Usability studies are important. You can use your internal resources or find some good resources to test your documentation and product.
Using non technical resources will delay not only the documentation publication process but stifles advancements or recommendations for the product.
Additionally, several orgs and corps are using Technical Marketing persons. Sometimes these are not so technical people while in other cases, these resources are very technical and are valuable to an organization.

The non technical fluff side is just not something I enjoy working with. I try to avoid this.
In the case of respin, the software was featured in LinuxJournal in early 2018.
There was never any advertising or marketing campaign. Nothing like this at all.
I'm not saying you don't need marketing or advertising, I'm saying we can use diferent aves.

Obviously, we don't want to fil out a form to try the product usually.
We don't want to give our information or allow cookies or tracking of our activity.
It's not cool.

Where linux devs are different is in that we will go to sourceforge or a repo and grab  your software or source. we might do it in a VM or use containers. However, when we need to enter our personal information or sign up, there is a distancing of sorts. 
I really must like your platform, area, product, etc in order to fill in this form with my legit information.



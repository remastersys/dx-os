# let's build a better mousetrap.

Mousetrap tracks  your eye movement and moves the cursor, allowing the user to select the item.

# why is this important?
As a community contributor for 2 decades, I realize there are many in our community who may need this type of tool.

## The tool needs to be improved.

- I, personally, need a tool like this - my daughter is deteriorating from a rare condition:
Mixed Connective Tissue disease. 

She's an adult now, but she could use a tool like this.

- Many with parkinsons.

- Could be other uses.

I really would like to put a call out to the community to help build a better mousetrap.

For me, it's a race against time.
The need for good accessibility tools will continue far past mine and my daughter's lifetime.
 
Thank you.

# where are the docs taking me?
where do you want to go?

# Some reasons developers will use your tool/app/utility
1. Need integration
2. Need results
3. Need solution
4. Need experience to level up
5. Need training to learn concepts

# End-to-End


# End user empathy

## Could you use the documentation and tool with no issues?

## Where do you get information about the need for support or quality improvements of the tool or docs?

# Maybe your dev process could improve
## Documentation will not improve your development process, but can pinpoint issues and root cause
## DX testing is not a substitution for good writing
## DX testing is not a substitution for validation and testing

The user experience is separate from the testing/validation process.

- The focus of developer experience testing is the measure of the experience a developer has during use of the product.
- The focus of quality assurance testing is the measure the features to the requirements and validate the product works period.


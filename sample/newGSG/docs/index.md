# Get Started with Linux Documentation

For full documentation visit [mkdocs.org](http://mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs help` - Print this help message.

## Start creating docs for your Linux Projects

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.
## Documentation
  - Front Matter: 00FM.md
  - Getting started: index.md
  - API: api.md
  - Chapter 1:
    - Introduction: 01Ch.md
  - Chapter 2 - Documentation Development Life Cycle:
    - Documentation Development Overview: 02Ch.md
    - Needs Analysis: needs_analysis.md
    - Audience Analysis: audience_analysis.md
    - Outline: outline.md
    - Prototype: prototype.md
    - Development: development.md
    - Review: review.md
    - Publish: publish.md
    - Maintain and Support: maintain-support.md
  - Chapter 3 - Types of Technical Documentation:
    - Types Overview: 03Ch.md
    - Style Guide: style-guide.md
    - Brand Guide: brand-guide.md
    - User Guide: user-guide.md
    - Installation Guide: install.md
    - Get Started Guide: gsg.md
    - White Paper: whitepaper.md
    - Application notes: app-note.md
    - Product Brief: brief.md
    - Product Specification: spec.md
    - Reference Implementation: refimp.md
    - Data Sheet: datasheet.md
    - Internal design docs: designdocs.md
    - Release notes: release-notes.md
    - Errata blues: errata.md
  - Chapter 4 - Legal:
    - Contract: contract.md
    - Contributing: contributing.md
    - Copyright: copyright.md
    - Legal: legal.md
    - Licensing: licensing.md
    - Terms of Service: tos.md
    - Trademark: trademark.md
  - Chapter 5 - Training and Tutorials: 04Ch.md
  - Chapter 6 - Experience: 05Ch.md
  - Chapter 7 - Maintaining and Support: 06Ch.md
  - Chapter 8 - Conclusion: 07Ch.md
  - Chapter 9 - Common Words and usage: 08Ch.md
  - Version: methods/types.md
  - License: license.md

# Extensions
markdown_extensions:
  - admonition
  - codehilite:
      guess_lang: false
  - toc:
      permalink: true

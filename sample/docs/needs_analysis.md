# Needs analysis

# We need documentation! or do we?

# Why 
## List reasons why here
- man page vague
- some users do not read man pages
- options not self explanatory

# Justify this need
# Proposed timeframe
# Proposed team members and contribution percentage or hours
## Technical Writer/Editor 20 hours a week
## Developer 5 hours a week
## Project Manager 1 hour a week (basic task management - could be managed by technical writer or developer alternately)
## Testing - Validation and review - depends on project size and scope
Example:
# Internal testing - new software product with hardware
## Validation
Use internal resources to conduct validation/testing
Some examples include: interns, other developers, people not familiar with the product but with the developer background, anyone able to perform the validation. 

### Offer incentives - caffeine, sugar

### Proposed Environment
#### External and internal noise
Temperature control

Noise Control

Eliminate Distractions

Make the participant comfortable

### How will we Capture data

- Observation
- Video capture participant
- Video capture screen
- bash history or keylogger
- click maps
- eeg

### Other considerations
- accessibility
- privacy
- security

## Participant testing
Official developer testing at an outside lab
Enter any info here time and location:

# Proposed documentation
## Installation Guide
## Get Started Guide
## User Guide
## Release Notes
## Sys Admin Guide
## Tutorials
## Video demos
## Reference Guide
## Quick Start Guide
## Reference Card
## Application Notes
## Product Brief
## Product Specification
## Whitepaper

## For training
### Facilitation Guide
### Student Guide

# Proposed Video/audio
## podcast
## streaming live video interactive webinar
### make sure full disclosure
I once went to a security IOT webinar - it was windows... that was sad. I was sad.
Disappointing
## live demo
## recorded demo
## twitch stream
## audiobook
I don't know anyone who has audio books for user or get started guides but.... have the accessibility options

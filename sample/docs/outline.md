# Recommended

Sample: Will provide - to do

# Brainstorm topics
Whichever way works for you

- notes
- lists
- outside perspectives

# Organize topics:

- chronological
- functional
- component
- other category type

# Example Get Started Guide

To get you started, try some of these sections

1. Documentation or Contents - Level 1
2. Run an application - Hello World
3. Modifications/configurations
4. Projects and Tutorials
5. Next Steps

# Introduction (required)
Introduce the project/product/topic of interest. List what will be discussed.
What is it?

# Objectives (optional)
* Never enumerate
* Use bullets
* Use action verbs

Upon successful completion of these instructions, you should be able to:

Understand how time/date stamps work
Configure the script to include a time/date stamps in python
Run inference with time/date stamp output

# How It Works (optional)

This section describes:

- Design summary
- Architectural Diagram
- Components
- Services

## Get Started (required)
Describe the basic steps for the guide.

# Section Heading such as Install (required)

## Before you begin (required)

- Describe the prerequisites
- List requirements outside of the device hardware

### Hardware requirements (required)
Add information for device hardware.

- Processor Information
- RAM
- Storage minimum
- Internet connection

### Software requirements (required)
Add information for software.
- Operating System
- Dependencies
- Other packages

#### Do not list default to the particular OS
Do not lead users to install 3rd party software unless necessary

Example: Do not lead users to install Etcher on Ubuntu when startup disk creator will do the job just fine

## Steps (required)
Include the steps needed to complete the task.
- Install prerequisites (required)
- Run (required)
- Stop (optional)
- Clean (optional)

# It Works! (hello world)
- Describe the sample or introductory exercise
- Include an architectural diagram

## Modify Steps (optional)
Include the steps needed to try for yourself.
- Install prerequisites (required)
- Run (required)
- Stop (optional)
- Clean (optional)

# Tutorial(s)
- Describe the tutorial(s).

## Tutorial 1: (required)
### Title of Tutorial (required)
- Describe the purpose of the tutorial.
- Add an Architectural Diagram.
- Include Objectives (upon successful completion)

### Steps (required)
- Include the steps needed to complete the tutorial.

#### Step 1: Name of Step

- Describe the step.
- Include step instructions

#### Step 2: Name of Step

- Describe the step.
- Include step instructions

#### Step 3: Name of Step (optional)

- Describe the step.
- Include step instructions

### Additional tutorials (optional)

### Summary (required)

- Discuss tutorial(s)

# Next Steps (required)

- Describe where to next
  Example: Relevant demos, similar projects

## References

- URLs to other documentation.

## Code Samples

- URLs to code samples or repos.

## Additional Information

- URLs for additional information.

## Links to other resources

- Links to additional resources.

# Troubleshooting (optional)

- Include any troubleshooting tips.

# Legal (optional)

- Include Legal information.

## Tell them
1. Intro: Tell them what you are going to do
2. Body: Tell them how to do it
3. Summary: Tell them what you did
4. Next Steps:Tell them what they can do next

GFDL: https://www.gnu.org/licenses/fdl-1.3.en.html

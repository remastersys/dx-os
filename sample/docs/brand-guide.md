#Brand Guide

- What it is
- How it works
- Contents

# Color palette

# Typefaces/Fonts

# Images

# Copyright
## Free Culture
## Public Domain
## permission


# Trademark and Licensing
-------- -----------------
## 1. Logo
size, style, samples

Placement
Examples good, bad, ugly

## 2. icons/symbols
Examples
Usage

## 3. Licensing terms - generic

	"for more information, ... "



# Photography

# Audio

# Social Media

# Brief Writing

# Presentations

# Learning materials
## webinar
## tutorial
## internal
onboarding
other
## external
youtube/vimeo policies


# web standards
## accessibility
## cms specific

# Your strategy
Your tagline/brand explained

# physical assets
Collateral material - printed
Swag

# vendor policy generic
contact us
a list of vendors for internal Usage

# resources

# photo release

STAY INFORMED!

# review process

# Prior to publishing
## Marketing Communications - ad
Handle external publication - reviews materials
### Creative people - images, banners, spinners

### Liberal Arts - branding

## Technical Communications
Tech people here
### Technical Writers
### Technical Editors
### Video content editors

## DX (can be technical writers, must be technical if the content is technical)
Reasons - if the person is non technical, it really is a worthless effort
The non technical staff working on technical docs cannot:

- diagnose and analyze
- Find solutions during testing
- Prepare testing materials
- Select qualified developers
- Evaluate and analyze
- Use effective tools
- Submit valid results

and most of all... really
- CANNOT MAKE PROPER RECOMMENDATIONS!

# Free Software Foundation
POC: RMS
## GNU
## gcc
https://www.gnu.org/prep/standards/html_node/Trademarks.html

2.3 Trademarks
 - Please do not include any trademark acknowledgments in GNU software packages or documentation.

 - Trademark acknowledgments are the statements that such-and-such is a trademark of so-and-so. The GNU Project has no objection to the basic idea of trademarks, but these acknowledgments feel like kowtowing, and there is no legal requirement for them, so we don’t use them.

What is legally required, as regards other people’s trademarks, is to avoid using them in ways which a reader might reasonably understand as naming or labeling our own programs or activities. For example, since “Objective C” is (or at least was) a trademark, we made sure to say that we provide a “compiler for the Objective C language” rather than an “Objective C compiler”. The latter would have been meant as a shorter way of saying the former, but it does not explicitly state the relationship, so it could be misinterpreted as using “Objective C” as a label for the compiler rather than for the language.

Please don’t use “win” as an abbreviation for Microsoft Windows in GNU software or documentation. In hacker terminology, calling something a “win” is a form of praise. You’re free to praise Microsoft Windows on your own if you want, but please don’t do so in GNU packages. Please write “Windows” in full, or abbreviate it to “w.” See System Portability.

IAGS standards - asterisk everything... no. It's disrespectful to the FSF recommendations and the FSF. See "learn about our community ffs"

- Question: Can trademarking a brand that is NOT trademarked and is against the request of the party, knowingly... now - that it is NOT trademarked - be a liability and damaging?
  - ... I tried
  -¯\_(ツ)_/¯

# Mozilla
http://www.mozilla.org/en-US/foundation/trademarks/list

# Other
## Postgres:
POC: Josh Drake

## Linux Found
Source/POC: pages and Jim or Angela

## Let's Encrypt
POC: Josh Aas

## Canonical

POC: None -
Trademark guidance on website

If Canonical is one of your partners, and you have it in writing – somewhere, can use
If Licensed, can use logo
If you get it in writing, you can use the logo. However, you are not supposed to use the logo to imply endorsement.

•	You can use the trademarks with Canonical permission in writing.
•	If you require a trademark license, contact them.
•	You can use it in fair use as long as it doesn't imply endorsement: parody, commentary, discussion, criticism
•	You can write articles, create websites, blogs or talks about Ubuntu, as long as you are not speaking on behalf of Canonical or imply endorsement

You probably may have a better POC but here is the contact page

## Docker
Previous POC - Jeff Nickoloff
Current: Website guidance

### PROPER TRADEMARK USES
Any permitted use must not falsely imply or suggest a sponsorship or endorsement by, or a partnership or affiliation with Docker. We reserve the right, in our sole discretion, to determine when this condition is met or violated.

Use of Logos
The Docker’s registered, unregistered and pending logos, including the Docker “Moby Dock” whale logo, as well as other design elements such as characters and container images, may be taken only from Docker’s product sheet or from Docker’s service screen shot following authorization from us.

Do not create imitations, facsimiles or reproductions of these design elements on your own. The design elements may only appear with the words, logotype, graphic elements and spacing which Docker has previously approved for use and may not be modified, recombined or displayed in modified scenes, images or other artwork.

Our word Marks (but not logo marks or other graphic depictions of our Marks) may be used in an informational context to name or describe the subject matter of an educational or informational program, to refer to Docker product or technology, provided however that such use otherwise complies with these Guidelines and the following requirements:
•             The Docker mark is used only in a referential context or for naming Docker or to indicate compatibility, and not in a title of a program, domain name, website, product or service
•             The use must not falsely imply or suggest a sponsorship or endorsement by, a license from, or a partnership or affiliation with Docker.
•             Any printed or online materials relating to the program must include proper attribution statements, registration symbols such as ® or © as appropriate, and a trademark notice statement in a form that we provide or approve.

### TRADEMARK NOTICE STATEMENT (should you have permission)
The website should display a legal notice or a link that contains the following legend:
“Docker and the Docker logo are trademarks or registered trademarks of Docker, Inc. in the United States and/or other countries. Docker, Inc. and other parties may also have trademark rights in other terms used herein.”
This legend can be modified to include only our Marks.

## FFmpeg
website


## GStreamer

in irc and twitter
have notes

## VMWare EXSi
VMWare website

## Neptune
Deprecated web page

## KVM
There's Intel(r) KVM
and there's KVM

## Amazon AWS
Larry
website

## bash
stop

## Azure
See MS guidance

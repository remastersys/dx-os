# Use existing linux tools

- scrot
- .bash_history

To evaluate commands and computer input behind the scenes - while the user is doing dx
## Audio application or allow audio commentary by test participant
        To capture audio
        - xvidcap
        - simple screen recorder (both audio and video)

## Eye tracking
        To capture users eye movements  
        - pygaze
            06022019 results
            Pygaze not clean install on Unbuntu
            Check old 2016 server - Debian for working code
            Alternately, visit DX lab at ASU - could be on donated image - Debra
        - Other eye tracking
            Needs development
            Works on a heat map
        - motioneyes
            No pi testing currently to see what cristian has done in the past few years

## Mood tracking
        To capture user mood/emotion
        - create sample using IR for mood - an Intel pretrained model

## ClickHeat
        To capture click data
        - Install Wordpress on test machine
        - Install clickheat
        - post instructions using this
        - Get your data

        Click heat - not tested
        Research to mouse click and desktop area of click"age"

## EEG
    To capture brain activity during test
        - Dolby
        - ASU iLUX

## Feedback form --
        To get feedback during or after
        Let user click a feedback button to enter content and capture ss during the process.
        Could be timed - user enter feedback at 1min10sec - && at 1min10sec user was on this screen. Would need ffmpeg - time of video capture- then take a ss of that time..

## Help/support chat - like an emergency button or some guidance
### What are the guidelines at reaching a point where users need help
    example, even though images clearly show which number is container id... user is at a block

## Smart survey

## Adaptive after questions

### Are you a Linux developer
    Language
    Years
    Distro
        Ask distro centric basic questions
        i.e. Ubuntu
            Do you know what company for Ubuntu?
            Do you know any other Ubuntu Developers?
        What is your IDE?
        Which shell?
            If the user says - All of them... Houston - we have a problem.
        What do you use to open a tar.gz file
        what do you use for xz
        Which build tools do you use?



## Boxen Setup

    - [x]  multiple users
    - [x] each has its own history
        - [ ] upload bash history of various users (cron or manual) Cron Preferred
        - [ ] run script to find correct commands in order
        - [ ]  note - any anomolies
    - [x] run scrot at beginning of dx testing ([ ] add specific duration of test)
        - [x] push images (mv) to folder (5 seconds currently)
        - [x]  cron later uploads to evaluation cloud/site
        - [ ]  run script to organize and evaluate
    - [ ] take results and add to a dashboard

# Create Image for DX testing using Libre Respin
    https://gitlab.com/remastersys/LinuxRespin
    http://www.linuxrespin.org/

## script - install your dxos depends ##
### author: mkw ###

#!/usr/bin/env bash
shopt -s extglob
set -o errtrace
set -o errexit
function dxos_update {
  sudo apt-mark hold grub2-common grub-common grub-pc grub-pc-bin
  sudo apt-get -y update
  sudo apt-get -y upgrade
}
function install_pkgs {
  # packages in list: curl to where your list is ../master/your_pkgs
  # example:  software=( $(curl -sSL https://raw.gitlab.com/remastersys/dx-os/master/packages.txt | sed '/^ *#/d;s/#.*//' ) )



  software=( $(curl -sSL ../master/dxos_pkgs) )
  for package in ${software[*]}
  do
    sudo apt-get -y install $package
  done
}
function depends_check {
  dxos_update
  install_pkgs
}

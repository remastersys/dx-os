# name: privacy's not public aka pnop
# author: marcia wilbur - mkw
# date: 2020
# version 0.1-0
# license: GPLv3
# this is a form app for privacy selections for participant testing

yad --title="privacy" --text="Please select and enter from the fields:" \
--image="/usr/share/icons/Tango/scalable/emotes/face-smile.svg" \
--form --date-format="%-d %B %Y" \
   --field="lang": en \
   --field="title": DeveloperX \
   --field="name": name \
   --field="pub-date":DT \
   --field="Video Recording":CBE \
   "" 'Internal!Share-Project!Public!Other' > /tmp/entries
   --field="Last holiday":CBE \
   "" 'Gold Coat!Bali!Phuket!Sydney!other' > /tmp/entries
   --field="license":CBE \
   "" 'GFDL!Public-Domain!CC by 4.0!others' > /tmp/entries

# Log Files - Bash history
--field="bash":CB \
"" 'No-sharing!internal!-with-pidgin!Public' > /tmp/entries

--field="extracted-img":CB \
"" 'N/A!internal!share-with-pidgin!Public' > /tmp/entries

--field="personal":CB \
"" 'No-sharing!internal!share-with-pidgin!Public' > /tmp/entries
# /tmp/entries
lang=$(cut -f1 < /tmp/entries)a,e
title=$(cut -d'|'-f2 < /tmp/entries)
vidcap=$(cut -d'|'-f6 < /tmp/entries)
extracted-img=$(cut -d'|'-f7 < /tmp/entries)
bash=$(cut -d'|'-f9 < /tmp/entries)


final=$(echo $lang $vidcap $pubdate $extracted-img + $bash\ | yad --text-info --editable --width="300" --height="300" --text="output (editable)")


# add tags
# add manifest start/stop

cat /tmp/entries >> /home/ieisw/formtest/outfile

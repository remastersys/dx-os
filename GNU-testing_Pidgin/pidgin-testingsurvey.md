# Survey (ish) Draft

## Testing information
1. how long did the test of the software take - estimate if you had to
2. which operating system did you use?
3. Did you encounter any errors?
4. Do you have feedback?

## Participation questions
### Level:
Had you ever heard of pidgin or adium before?
Have you ever used pidgin or adium before?

### Technical experience:
How many years have you been using the operating system?
Do you consider yourself technical? Which level best fits [ ]beginner [ ]intermediate [ ]expert
Any other technical experience you would like to share?

### Opinion
What do you think of the software (Pidgin or Adium)?
Any other feedback?

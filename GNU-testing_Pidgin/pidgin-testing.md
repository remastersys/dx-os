# Pidgin Testing Guidance - May 2020

# Pidgin Testing Overview

The pidgin testing should take less than a half hour to complete.
For this testing, we will be testing the website:

pidgin.im

# What we test

The participant will use the website to get the application and install the software packaged based on the OS/distro.

# Tools for testing
There are 3 options. Use your system or X2Go into ours.

## X2Go
Contact us to get credentials set up

## Image - VM or live
We are working on an image which at this point is ready to be used as soon as Marcia gets respin fired up!
Request live and we can give you the WIP

## Testing on Your System
For the purposes of GNU testing, the following guidance is provided if using your own machine.

### Capture Software
	Open Broadcast Studio (preferred) - your screen, audio and webcam capture
	Simple Screen Recorder

### Configure bash_history
 Instructions here to capture multiple terminals in real time or just use one to capture the content.
 When you are done, we request the .bash_history for the time frame of the testing.
 you can find this in /home

#### Edit .bashrc

Add the following edits to .bashrc:

    shopt -s histappend

They are used to extend bash history beyond the default 500 lines.

#### Edit HISTSIZE AND HISTFILESIZE

| Term | Description |
| ---------- |-------|
| **HISTSIZE**      | lines stored in memory for the history list while a bash session is ongoing. |
| **HISTFILESIZE**  | lines allowed in the history file at startup time of a session; stored in the history file at the end of a bash session future sessions.|

    HISTSIZE=5000
    HISTFILESIZE=10000

To preserve bash history across multiple sessions:

    export PROMPT_COMMAND="history -a; history -c; history -r; $PROMPT_COMMAND"

# Get Started
## Step 0. Run OBS or Simple Screen Recorder
Capture the screen and record.

## Step 1. Go to pidgin.im

  ## Install Pidgin

## Step 2. Configure pidgin
## Step 3. Use Pidgin - first use and configuration
## Step 4. Stop recording and send/transfer
## Step 5. Take survey
Include any notes you may have for areas of challenge or improvement or feedback

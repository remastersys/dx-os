# Recording Licensing and Privacy
At GNU Essentials, we respect your privacy and this agreement sets terms you are comfortable with.

# Content License for recording:
[ ] GFDL

[ ] Public Domain

[ ] CC

If you have no privacy selections, skip this section.

# Privacy and use:
We like to share results with the project - In this case,
You select the terms you are comfortable when it comes to sharing the Participant Capture/recording(s).

## Video Capture recording
[ ] Only internal to testing - does not include external projects

[ ] Share recording with the project (Pidgin)

[ ] Public

[ ] Other terms:

## Audio capture recording
[ ] Only internal to testing - does not include external projects

[ ] Share recording with the project (Pidgin)

[ ] Public

[ ] Other terms:

## Webcam capture recording
[ ] N/A

[ ] Only internal to testing - does not include external projects

[ ] Share recording with the project (Pidgin)

[ ] Public

[ ] Other terms:

## Extracted Images from recording
[ ] Only internal to testing - does not include external projects

[ ] Share recording with the project (Pidgin)

[ ] Public

[ ] Other terms:

# Personal information
[ ] Do not share my name or personal information

[ ] Use my alias ______________

[ ] Call me anything

[ ] Other terms:

# Log Files - Bash history
[ ] N/A

[ ] Only internal to testing - does not include external projects

[ ] Share recording with the project (Pidgin)

[ ] Public

[ ] Other terms:

# Acceptance of this Policy
I agree the following terms were not coercive and are the terms under which I would prefer for the content.

# signature

name

date

# Common word list - FOSS edition

## Other
List any here

## These are used sometimes differently within a document:

- console
- terminal
- xterm
- shell

## sdX
usage sdX - soooo important!!!!!!!

See - example sdX

Explicitly declaring a drive can be a liability - need a disclaimer and better to use something like sdX

## Command (functional) vs. Not a Command (product)

### Example

firefox
- on the command line, functional: no registration mark, this is a command
- as a product, not functional: Firefox, needs Trademark/Brand

This applies to other applications, tools, and utilities that can be called at the command line:

- ffmpeg
- vlc
- audacity

# Terminology

**audacity** - usage needed

**copyleft** - Richard's house

**crontab** - cron table

**Creative Commons** - Larry's house

**cURL** - This is the correct usage in text. The command is curl.

**distribution** or **distro** - flavour of Linux

__FFmpeg*__	- Fast Forward mpeg: A free software project that is a leader with their multimedia framework used to decode, encode, transcode, mux, demux, stream, filter and play.

**FOSS** or **foss** - Free and Open Source Software

**Free Culture** - Larry's licensing stuff

**GCC** - Confirmed with rms - no. According to gcc documentation - No trademarks. Even requests NO trademarks.

**GFDL** - RMS house content license from the FSF
-Link to
-Usage
-Example

**GPL'd** - I used Richard's house recipe

**GPL** - RMS's house licenses from the FSF
-Link to
-Usage
-Example

__GStreamer*__	An open source project that provides a library for constructing graphs of media-handling components. GStreamer playback, audio/video streaming as well as audio (mixing) and video (non-linear editing) processing.
* Note: according to the devs, there are several different content licenses - so with regard to definition, etc. - Define the licenses and usage.

**image** n. - a graphic

**image** n. - A file that contains all information needed to produce a live working copy. *.img *.iso

**Jekyll** - ruby based (more)

**LaTeX** (pronounce it Laaaaa Tec)

**localhost** or Localhost or LocalHost or --- 127.0.0.1: Preferred localhost
    Never end a localhost path with a period
    localhost/mycloudserver = ok
    localhost/mycloudserver. = no

**midnight commander** or **mc** - Definition: Best app ever created on the planet! File manager for GNU Linux distribution

**NVMe** - (more)

**Open Source** - as Richard once said to me... "those open source people..." It would be ideal if the writer/author/editor and else knew there is a difference between open source and free software and what that difference is.

**PHP** - scripting (more)

**Python** (more advanced scripting - Did you know.. the RH installer was written in python... OMG I'm old)

**pwm** - lightweight window manager

**RESTful** - (more JSON)

**ReST** - restructured text - used with sphinx

**Ruby** - (more gems of wisdom)
Unfortunately uses censorship covenant

**sphinx** - (really awesome but I had to write a script for links when doing md2html)

**qsv** - (more)

**Qt WebEngine** - https://wiki.qt.io/QtWebEngine - usage

**twm** - really greeeeen window manager

**wayland**

**weston**

**X** (The **X Window System**)
    X11
    X Window System
    Xorg
    XFree86.config (just kidding... ah the good old days)

## Usage: Directory and File

directory, not folder

within (a directory, in a file)

## Usage: In - vs. another term

In looks too much like ln, especially in cases where there is code next to it.
Reduce usage. Use another term when possible.

## Style: Use courier for directory paths
Or some other font that is used for code snippets.

## Style: white is so hard on the eyes
If possible, use a background this is accessible friendly, offer contrast options, and not bright white.

## Usage: Save the file. No need to save and close.
This is Linux. I keep my files open unless I'm sure I won't be needed to use it later.

## Style: Don't use gedit with the colored text.
Change profile for another color if taking screen captures to show what something should look like.

## Style: Look at international guidance for using red
It's about 2 decades old.

# Also - Acronyms
In addition to a common word list, a list of acronyms and usage list are available.

This style guide is not a dictionary. The goal here is to give guidance about usage, not to define the terms in detail.
Each term in the guide earned a place, some earned a place here more than others! :)

How words were selected:
 - Prior evidence of need
 - Could be a homonym or used more than once ... ex. ssd and ssd (see also Prior evidence of need)
 - May be uniquely written
 - Meaning confusion
 - Standard to follow based on project requirements ex. FFmpeg and ffmpeg
 - Common usage - ex. crontab is the common usage, not cron table  

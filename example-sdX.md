# Clearly guide authors and writers to use sdX rather than explicitly declare "sdb" or "sda"
In some cases, explicitly declaring the drive could be not only a liability but considered somewhat reckless or not cautious. Users may be copying and pasting the content and hose their systems.

## The order for checking for a drive is usually:

df -h or fdisk -l

umount /dev/sdX
(where sdX is the USB)

sudo dd if=fs_image.img of=/dev/sdX bs=4M; sync
(where sdX is the drive, also use status=progress to see status)

## Public Facing example
find one - let them know "needs edits"

Note: I know a little bit about custom images -mkw  :D

# this is our journey


As developers, we focus our efforts on our code, our software/tools/utilities, etc.
We improve our features and add value to our software.

Documentation is an important piece of this overall picture.
Useful and usable documentation is essential.


## Documentation Development Life Cycle

- Infographic (doclifecycle-mkw.jpg)

- Needs Analysis (needs_analysis.md)
- Audience Analysis (audience_analysis.md)
- Outline (outline.md)
- Prototype - Template document, style (doc-templates/)
- Develop Content (developdocs.md)
		content
		trademark (trademark.md)
	 	copyright (AMA)
		licensing (multiple resources - licensing.md)
- Review (review.md)
-	Publish (publish.md)
-	Maintain Support (maintain-support.md)

## How to Improve the Documentation processes

	Templates (doc-templates)
	git
	markdown
	open source process over proprietary
	LaTeX
	asciidoc
	pandoc
	jekyll
	sphinx
	libreoffice
	dita
	docbook
	other: proprietary

## Collaboration Alternatives for Documentation

	Etherpad - cross teams experience with Free Culture trust
	Versioning
	Proprietary - Google Docs
	Nextcloud - how nc streamlined our project and saved us buckets of time
		Ventrillo
		irc
		slack

### What is a solution?

Solution = right people, right tools, right skills

I have seen some microservices focus for software and platforms.
Recently, I worked a short gig for a company out of Silicon Valley focused on microservices.
The tech writers didn't even know how to generate a key pair.

Remember:
Right people, right tools, right skills...


## Useful and Not so useful
	- POSIX info
		- Dash
		- Quote


## The importance of validation.
Validation can pinpoint opportunities for Quality and Usability improvements.

## QA v DX
	Description of differences

	Documentation can improve your development process and pinpoint root cause at times

	DX testing is not a substitution for good writing

	DX testing is not a substitution for validation and testing

### User or Developer Experience

The user experience is separate from the testing/validation process.

### Difference between Developer Experience Acceptance Testing and Quality Assurance Testing

* The focus of developer experience testing is the measure of the developer experience during use of the product.

* The focus of quality assurance testing is the measure the existing features to the functional requirements and validate the product works period.

## Command v Brand (trademarks.md)

Examples:

|Brand	       |             command|
|--------------|--------------------|
|FireFox	     |             firefox|
|Chrome	       |              chrome|
|VirtualBox	   |          virtualbox|
|LibreOffice	 |         libreoffice|


This is why we need either Review... or Linux writers to write for Linux products/software etc.

- I have seen trademarks doctored in images showing commands in the terminal.
      firefox(r)

- I have seen trademark referring to the terminal like this:
      terminal(r)

## Tools, utilities, apps in the FOSS Content Creator Toolbox

|Application/Tool/Utility	|Name |	Function|
|-------------------------|-----|---------|
|scrot|	SCReenshOT|	screen capture in the background|
|nano	|Nano's ANOther editor|	quick editing - free Pico clone|
|Eclipse|n/a|IDE
|atom.io|	n/a	|markdown/code editor
|gedit|n/a|text editor|
|midnight commander|mc|file manager and so much more!
|gimp	|GNU Image Manipulation Program|	photo editor|
|simplescreenrecorder|Simple Screen Recorder	video |capture tool|
|obs|Open Broadcaster Software|recording screen and camera capture
|kdenlive|KDE Non-Linear Video Editor|quick video editing tool|
|xvidcap|n/a| capture video screen - misuse of the X window system term on sourceforge http://xvidcap.sourceforge.net/#intro|
|recordmydesktop||capture video screen|
|vlc|VideoLAN|Crop video etc.
|x2go|n/a|remote desktop|
|openssh|n/a|ssh|
|vnc|	Virtual Network Computing	|remote management (for small board computing like pi*)
|bash|	Bourne Again SHell|	shell and command language

## Tools, utilities, apps for DX Participant Testing (dxos.md)
### obs
With obs, you can use the linux computer the participant is working on to capture video and audio.

With dx-os, this is automatically configured for capture.
Configuration of settings for the audio device may be required.

Instructions for configuration of audio:

Instructions for adding a stream key:

Instructions for adding/removing mask:

Instructions for easy remote participant testing:

Validity improves as no one leading the participant.

### image
    will be available on sourceforge

### package to come
    Debian Based

These are tools used to conduct participant testing of Linux instructions and documentation.

* To be used on Linux for Linux docs by Linux leads

* Otherwise, the testing is for naught and makes no sense

# Components
##  Bash History

    bash history setting edits

    Outfile auto generated at the end of a session - give the people a button or something

Demo provided for this presentation

## Screen capture the participation session

### OBS installed and configured

Can even have handshake to view the screen... not with external camera but livestream the user moving the mouse etc.

1. use websocket to control OBS from a phone or tablet on the same network...
https://github.com/Palakis/obs-websocket

2. view stream as a "viewer only" from wherever

APIs are developer friendly

### scrot with time stamp for screen capture
### video capture of participant - is it needed?
does this make participant uncomfortable
what data - useful data comes from taping a subject during the testing

## Track user interaction
### Clickheat map
### Other maps - scrolling

## Eye tracking
could be useful

## Emotion computer vision
models can be used to determine whether a participant is confused or upset or even mad

## timing notifications or elapsed time
If a task takes more than a few seconds, perhaps a notification or chatbot will be presented

## Flash Cards
we can start with flash cards for definitions to understand where the participant is starting from
in other words, who is this audience...

## card sorting
## Electroencephalography (EEG) in Linux DX participant Testing

Did you know you can order 50 leads and make your own?
I started this in Feb but, never got around to it.

Basic explanation
Service offered  by...
used by ...
was it sony or thx


## remote testing capabilities

# Privacy and Participant agreements
## what will the data be used for
## create a template
For example, why was I able to see an unflattering image of a participant in the 2nd round of testing

# Security

Example: Don't send your readers to a non secure website to download an iso

are you crazy?!

# Context
## Types of Content - Media/context
### 1. Web Content
### 2. Standalone Applications

# Considerations
## Accessibility audit testing
audit - browser plugin
start with web
move on from there.

### tips for writing for accessibility
* alt tags

* never use "see" "location of button" "that blue thing" "on the left"

* video cc

* text to speech?

* jaws

* contrast in images

* text - font/style


# Developer Friendly

## SDK
not in scope

## API
not in scope

## Demo
we do not publish vaporware. Say it with me.

We DO NOT publish vaporware.

 Thank you for not wasting my freaking time!

## Code sample
Let me try your demo!

This is going to be fun tone.
Accomplish, achieve, reinforce learning.

## Privacy matters for participation
All your data are belong to us?

## CTA - Community
### Accessibility
    If you can add accessible friendly options or features, please do
### Open Collective over SFC
      https://opencollective.com/
### OSI voting
    - this year we rejected board members with censorship initiatives. we need more members for next year's vote.


Thank you.

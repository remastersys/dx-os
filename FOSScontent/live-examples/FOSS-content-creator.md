# Links
## FOSS Content Creator: Working with GNU Linux* Developers to Simplify Documentation

https://software.intel.com/en-us/blogs/2019/12/24/foss-content-creator-working-with-gnu-linux-developers-to-simplify-documentation

## FOSS Content Creator: Writing for GNU Linux* using GNU Linux*

https://software.intel.com/en-us/blogs/2019/10/05/foss-content-creator-simple-command-and-script-to-check-for-windows-characters-in

## FOSS Content Creator - Documentation Development Life Cycle for GNU Linux projects

https://software.intel.com/en-us/blogs/2019/07/13/foss-content-creator-documentation-development-life-cycle-for-gnu-linux-projects

## FOSS Content Creator Series: Simple Scripts - Screen Capture with scrot*

https://software.intel.com/en-us/blogs/2019/01/11/foss-content-creator-series-simple-scripts-screen-capture-using-scrot

## FOSS Content Creator Series: Is your content considered "Free Cultural Works"?

https://software.intel.com/en-us/blogs/2019/01/11/approved-for-free-culture-works

# Examples 

## Article uses ffmpeg command

https://software.intel.com/en-us/articles/extract-images-from-intel-free-culture-videos-for-inference-with-ffmpeg

## Article uses date stamp with computer vision

https://software.intel.com/en-us/articles/add-a-time-date-stamp-to-your-output-files-for-opencv-frames-in-python

## First Free Cultural Works release - Videos to test computer vision inference (various models)

https://software.intel.com/en-us/articles/overview-of-sample-videos-created-for-inference
